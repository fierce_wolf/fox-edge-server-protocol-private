# 介绍

这是一个开发自己的通道服务和协议解码器的工程范例


## 背景

**Fox-Edge** 是一个开放性的边缘计算平台和框架，支持开发者在基于Fox-Edge开发自己的设备和解码器。

但是，很多开发者从开源仓库Gitee上拿到完整的Fox-Edge-Server项目代码之后，面对Fox-Edge-Server庞大的代码
误以为需要对这些开源代码，进行的大量修改。，然后，浪费了不少个人的工作时间。

其实，基于在Fox-Edge开源框架之上进行开发，跟SpringBoot之上进行开发，方式并没有什么两样。
都是参考开源方提供的DEMO范例，建立一个独立的个人项目，然后再POM中参考DEMO的POM，引入必要的相关依赖，
就可以直接开发了。

所以，灵狐在这边直接新建一个独立的工程，作为DEMO范例，演示三种应用的开发。

## 模块

### 南向设备通道范例
**fox-edge-server-channel-simulator-service**

这个范例，演示了开发一个channel服务，应该要进行哪些基本的工作。更多的范例，参考其他channel服务。

注意：该服务是通道级别的仿真器，还可以帮助开发者进行设备收发报文的模拟

### 设备解码器范例
**fox-edge-server-protocol-haier-ycj-a002**

这个范例，演示了开发一个解码器模块，应该是如何进行开发的。

### 云平台对接范例
**fox-edge-server-iot-thingsboard**

这个范例，演示了跟上面的第三方云平台进行对接，应该是如何开发的。

### 本地调试服务
**fox-edge-server-device**

这是设备服务，当开发者开发完成设备解码器之后，将解码器的.jar文件，以xxxx.v1.jar的命名规范，放到
\jar\decoder下面

例如：

fox-edge-server-protocol-haier-ycj-a002-1.1.0.jar
放到\jar\decoder之下，命名为
fox-edge-server-protocol-haier-ycj-a002.v1.jar

并在本地启动gateway和manage服务后，将fox-edge-server-protocol-haier-ycj-a002配置为加载方式，
那么后面你就可以通过本地启动fox-edge-server-device，来调试解码器
fox-edge-server-protocol-haier-ycj-a002的代码了

### 远程调试解码器

在本地调试过后，可以将你开发的服务，放置到一台Fox-Edge系统之中。

然后，在IDEA当中建立一个**远程JVM调试**，再Fox-Edge系统上，使用命令行

./restart xxx/xxx -d192.168.xxx.xxx:5005 -p9xxx

启动对应的服务，就可以再IDEA远程连接到另外一台Fox-Edge上去调试了




